#!/usr/bin/env python3

import pathlib, json

import bs4

if __name__ == "__main__":
    filename = "data.html"

    file = pathlib.Path(filename)

    html = file.read_text()

    soup = bs4.BeautifulSoup(html, features="html.parser")

    table = soup.find("table")

    # headers = [next(x.stripped_strings) for x in table.thead.tr.find_all("th")]
    # print(headers)
    # exit()

    data = []
    for tr in table.tbody.find_all("tr"):
        datum = {}
        for i, td in enumerate(tr.find_all("td")):
            match i:
                case 0:
                    datum["name"] = next(td.stripped_strings)
                case 1:
                    datum["neurons"] = (
                        " ".join(td.stripped_strings)
                        if td.string is None
                        else td.string.rstrip("\n*^")
                    )
                case 3:
                    datum["method"] = " ".join(td.stripped_strings)
                case 4:
                    datum["sensory-associative structure"] = td.string.rstrip()
                case 5:
                    binomial = " ".join(td.stripped_strings).split(" ")
                    datum["genus"] = binomial[0]
                    datum["species"] = binomial[1]
        data.append(datum)

    with open("data.json", "w") as f:
        json.dump(data, f)
