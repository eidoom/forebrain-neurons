#!/usr/bin/env python3

import json

import matplotlib.pyplot

if __name__ == "__main__":
    with open("data.json", "r") as f:
        data = json.load(f)

    for i, datum in enumerate(data):
        data[i]["neurons"] = int(datum["neurons"].replace(",", ""))

    data.sort(key=lambda x: x["neurons"])

    for i in range(len(data)):
        data[i]["n"] = i

    isotropic = [
        x for i, x in enumerate(data) if x["method"] == "Isotropic fractionator"
    ]

    optical = [x for i, x in enumerate(data) if x["method"] == "Optical fractionator"]

    estimated = [x for i, x in enumerate(data) if x["method"] == "Estimated"]

    fig, ax = matplotlib.pyplot.subplots(figsize=(10 * 6.4, 4.8))

    ax.set_yscale("log")

    ax.bar(
        [x["n"] for x in isotropic],
        [x["neurons"] for x in isotropic],
        label="Isotropic fractionator",
    )

    ax.bar(
        [x["n"] for x in optical],
        [x["neurons"] for x in optical],
        label="Optical fractionator",
    )

    ax.bar(
        [x["n"] for x in estimated],
        [x["neurons"] for x in estimated],
        label="Estimated",
    )

    ax.set_xticks(
        range(len(data)),
        [x["name"] for x in data],
        rotation="vertical",
    )

    ax.set_ylabel("Number of sensory-associative structure neurons")

    ax.legend(title="Method")

    fig.savefig(
        "neurons.pdf",
        bbox_inches="tight",
    )
